package com.example.basiccalculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class CalculatorActivity : AppCompatActivity() {

    lateinit var tvExpression: TextView
    lateinit var tvResult: TextView
    lateinit var btnNumber: ArrayList<Button>
    lateinit var btnClear: Button
    lateinit var btnEqual: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)
        initializeView()
    }

    private fun initializeView() {
        tvExpression = findViewById(R.id.tv_expression)
        tvResult = findViewById(R.id.tv_result)

        btnNumber = ArrayList()
        for(i in 0..9) {
            var id = "btn_number_$i"
            var btnNumberX: Button = findViewById(getResourceId(id, "id", packageName))
            btnNumberX.setOnClickListener {
                var text = tvExpression.text
                tvExpression.text = text.toString() + i
            }
            btnNumber.add(btnNumberX)
        }

        var operator = arrayOf("plus", "minus", "multiple", "divide")
        for(op in operator) {
            var id = "btn_op_$op"
            var stringId = "button_op_$op"
            var btnNumberX: Button = findViewById(getResourceId(id, "id", packageName))
            var symbol = resources.getString(getResourceId(stringId, "string", packageName))
            btnNumberX.setOnClickListener {
                var text = tvExpression.text
                var expression = text.toString()
                if (expression.isEmpty()) {
                } else if (!isNumber("[0-9]", expression[expression.length - 1].toString())) {
                    tvExpression.text = expression.substring(0, expression.length - 1) + symbol
                } else {
                    tvExpression.text = text.toString() + symbol
                }
            }
            btnNumber.add(btnNumberX)
        }

        btnClear = findViewById(R.id.btn_clear)
        btnClear.setOnClickListener {
            clear()
        }

        btnEqual= findViewById(R.id.btn_equal)
        btnEqual.setOnClickListener {
            var expression = tvExpression.text
            if (!expression.isEmpty()) {
                if (!isNumber("[0-9]", expression[expression.length - 1].toString())) {
                    Toast.makeText(this, "Bad Expression", Toast.LENGTH_SHORT).show()
                    tvResult.text = ""
                } else {
                    // evaluation only + and -
                    tvResult.text = evaluateExpression(expression.toString())
                }
            }
        }
    }

    private fun evaluateExpression(expression: String): String {
        var operand = expression.split("(\\+|\\-|x|\\/)".toRegex())
        var operator = expression.split("\\d*".toRegex())
        operator = operator.filter { op -> !op.isEmpty() }
        // nOperand = nOperator + 1
        var result: Int = operand[0].toInt()
        for (i in 0 until operator.size) {
            if (operator[i] == "+") {
                result += operand[i + 1].toInt()
            }
            else if (operator[i] == "-") {
                result -= operand[i + 1].toInt()
            }
        }
        return result.toString()
    }

    private fun clear() {
        tvExpression.text = ""
        tvResult.text = ""
    }

    private fun isNumber(pattern: String, text: String): Boolean {
        return pattern.toRegex().matches(text)
    }

    private fun getResourceId(id: String, resourceName: String, packageName: String): Int {
        return try {
            resources.getIdentifier(id, resourceName, packageName)
        } catch(e: Exception) {
            -1
        }
    }
}
